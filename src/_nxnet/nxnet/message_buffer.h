#ifndef NXNET_MESSAGE_BUFFER_H
#define NXNET_MESSAGE_BUFFER_H

#include "message.h"
//#include "stb/stretchy_buffer.h"
#include <stdlib.h>

typedef struct NxnetMessageBuffer {
  NxnetMessage *messages;
  size_t length;
  size_t head;
  size_t next;
  size_t items;
} NxnetMessageBuffer;

void nxnet_message_buffer_free(NxnetMessageBuffer *self, int free_self) {
  if (self) {
    free(self->messages);
    if (free_self)
      free(self);
  }
}

NxnetMessageBuffer nxnet_makeNxnetMessageBuffer(size_t length) {
  NxnetMessageBuffer self;
  self.length = length;
  self.head = 0;
  self.next = 0;
  self.items = 0;
  self.messages = (NxnetMessage *)malloc(sizeof(NxnetMessage) * self.length);
  for (size_t i = 0; i < self.length; i++) {
    self.messages[i].kind = NXNET_MESSAGE_NULL;
  }
  return self;
}
/*
NxnetMessageBuffer *nxnet_newNxnetMessageBuffer(size_t length) {
  NxnetMessageBuffer *self = (NxnetMessageBuffer *)malloc(sizeof(*self));
  *self = nxnet_makeNxnetMessageBuffer(length);
  return self;
}
*/

void nxnet_message_buffer_add(NxnetMessageBuffer *self, NxnetMessage message) {
  self->messages[self->next] = message;
  self->items += 1;
  self->next = (self->next + 1) % self->length;
}

NxnetMessage nxnet_message_buffer_get(NxnetMessageBuffer *self) {
  if (self->items > 0) {
    NxnetMessage head = self->messages[self->head];
    self->messages[self->head].kind = NXNET_MESSAGE_NULL;
    self->items -= 1;
    self->head = (self->head + 1) % self->length;
    return head;
  } else {
    return nxnet_makeNxnetMessage(-1, NXNET_MESSAGE_NULL, "");
  }
}

#endif