#ifndef NXNET_API_PREFIX_NET_H
#define NXNET_API_PREFIX_NET_H

#include "../message.h"
#include "../socket.h"
#include "stb/stretchy_buffer.h"

static NxnetSocket *_nxnet_sockets = NULL;
static NxnetMessage *_nxnet_messages = NULL;
static int _nxnet_initialized = 0;

// typedef enum NxnetMessageKind {
//  NXNET_MESSAGE_NULL,
//  NXNET_MESSAGE_CONNECT,
//  NXNET_MESSAGE_DISCONNECT,
//  NXNET_MESSAGE_DATA,
//
//} NxnetMessageKind;

void nxnet_init() {
  if (!_nxnet_initialized) {
    stb_sb_add(_nxnet_sockets, 1);
    stb_sb_add(_nxnet_messages, 1);
    _nxnet_initialized = 1;
  }
}

void _nxnet_add_socket(NxnetSocket sock) {
  stb_sb_push(_nxnet_sockets, sock);
  stb_sb_add(_nxnet_messages, 1);
}

int nxnet_start_tcp(unsigned short port) {
  nxnet_init();
  NxnetSocket self = nxnet_makeNxnetSocket();
  int fd = nxnet_socket_tcp_start(&self, port);
  if (fd < 0) {
    nxnet_socket_free(&self);
    return -1;
  }
  _nxnet_add_socket(self);
  return stb_sb_count(_nxnet_sockets) - 1;
}

int nxnet_connect_tcp(const char *hostname, unsigned short port) {
  nxnet_init();
  NxnetSocket self = nxnet_makeNxnetSocket();
  int fd = nxnet_socket_tcp_connect(&self, hostname, port);
  if (fd < 0) {
    nxnet_socket_free(&self);
    return -1;
  }
  _nxnet_add_socket(self);
  return stb_sb_count(_nxnet_sockets) - 1;
}

int nxnet_tick(int socket) {
  nxnet_socket_tick(&_nxnet_sockets[socket]);
  return (_nxnet_sockets[socket].running);
}
void nxnet_send(int socket, const char *data, int target) {
  nxnet_socket_send(&_nxnet_sockets[socket], data, target);
}

int nxnet_receive(int socket) {

  _nxnet_messages[socket] =
      nxnet_message_buffer_get(&_nxnet_sockets[socket].messages);
  return _nxnet_messages[socket].kind;
}

int nxnet_kind(int socket) { return _nxnet_messages[socket].kind; }
char *nxnet_data(int socket) { return (char *)_nxnet_messages[socket].data; }
int nxnet_source(int socket) { return _nxnet_messages[socket].source; }

void nxnet_close(int socket) { nxnet_socket_close(&_nxnet_sockets[socket]); }
void nxnet_disconnect(int socket, int target) {
  nxnet_socket_disconnect(&_nxnet_sockets[socket], target);
}

void nxnet_ack_disconnect(int socket, int target) {
  nxnet_socket_ack_disconnect(&_nxnet_sockets[socket], target);
}

void nxnet_free_all() {
  for (int i = 0; i < stb_sb_count(_nxnet_sockets); i++) {
    nxnet_socket_free(&_nxnet_sockets[i]);
  }
  stb_sb_free(_nxnet_sockets);
  stb_sb_free(_nxnet_messages);
}

#endif