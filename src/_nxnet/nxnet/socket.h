#ifndef NXNET_SOCKET_H
#define NXNET_SOCKET_H

#include "message.h"
#include "message_buffer.h"
#include "stb/stretchy_buffer.h"
#include <arpa/inet.h>
#include <ctype.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define NXNET_READLEN 128

typedef enum NxnetSocketKind {
  NXNET_SOCKET_NULL,
  NXNET_SOCKET_CLIENT,
  NXNET_SOCKET_SERVER,
  // NXNET_SOCKET_CONNECTION,

} NxnetSocketKind;

typedef struct NxnetSocket {
  int fd;
  int running;
  NxnetSocketKind kind;
  unsigned short port;
  struct sockaddr_in address;
  int *connections;
  int *disconnections;
  NxnetMessageBuffer messages;
} NxnetSocket;

void _nxnet_add_disconnection(NxnetSocket *self, int connection) {
  for (int i = 0; i < stb_sb_count(self->disconnections); i++) {
    if (self->disconnections[i] == -1) {
      self->disconnections[i] = connection;
      return;
    }
  }
  stb_sb_push(self->disconnections, connection);
}

int _nxnet_has_disconnection(NxnetSocket *self, int connection) {
  for (int i = 0; i < stb_sb_count(self->disconnections); i++) {
    if (self->disconnections[i] == connection) {
      return 1;
    }
  }
  return 0;
}

void _nxnet_socket_disconnection(NxnetSocket *self, int connection) {
  for (int i = 0; i < stb_sb_count(self->connections); i++) {
    if (self->connections[i] == connection) {
      self->connections[i] = -1;
    }
  }
  nxnet_message_buffer_add(
      &self->messages,
      nxnet_makeNxnetMessage(connection, NXNET_MESSAGE_DISCONNECT, ""));
  // if (!acknowledge) {
  _nxnet_add_disconnection(self, connection);
  //}
}

void nxnet_socket_disconnect(NxnetSocket *self, int target) {
  if (self->kind == NXNET_SOCKET_SERVER) {
    if (target == -1) {
      for (int i = 0; i < stb_sb_count(self->connections); i++) {
        int connection = self->connections[i];
        if (connection >= 0 && !_nxnet_has_disconnection(self, i)) {
          nxnet_socket_disconnect(self, connection);
        }
      }
    } else {
      if (target >= 0 && !_nxnet_has_disconnection(self, target)) {
        close(target);
      }
    }
  }
}

void nxnet_socket_close(NxnetSocket *self) {
  self->running = 0;
  nxnet_socket_disconnect(self, -1);
  close(self->fd);
}

void nxnet_socket_free(NxnetSocket *self) {
  if (self) {
    stb_sb_free(self->connections);
    stb_sb_free(self->disconnections);
    nxnet_message_buffer_free(&self->messages, 0);
  }
}

NxnetSocket nxnet_makeNxnetSocket() {
  NxnetSocket self;
  self.fd = 0;
  self.running = 0;
  self.kind = NXNET_SOCKET_NULL;
  self.port = 0;
  self.connections = NULL;
  self.disconnections = NULL;
  self.messages = nxnet_makeNxnetMessageBuffer(128);
  return self;
}

void nxnet_socket_ack_disconnect(NxnetSocket *self, int connection) {
  for (int i = 0; i < stb_sb_count(self->disconnections); i++) {
    if (self->disconnections[i] == connection) {
      self->disconnections[i] = -1;
    }
  }
}

int nxnet_socket_tcp_connect(NxnetSocket *self, const char *hostname,
                             unsigned short port) {
  if (!self->running) {
    int tcp_sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    self->address.sin_family = AF_INET;
    // self->address.sin_addr.s_addr = INADDR_ANY;
    self->address.sin_port = htons(port);
    if (tcp_sock < 0) {
      perror("<!> ERROR creating socket");
      return -1;
    }

    int adress_err = inet_pton(AF_INET, hostname, &self->address.sin_addr);
    if (adress_err < 0) {
      perror("<!> ERROR address");
      return -1;
    }

    int connect_error = connect(tcp_sock, (struct sockaddr *)&self->address,
                                sizeof(self->address));
    if (connect_error < 0) {
      perror("<!> ERROR connecting");
      return -1;
    }

    self->kind = NXNET_SOCKET_CLIENT;
    self->running = 1;
    self->fd = tcp_sock;
    self->port = port;
    return tcp_sock;
  } else {
    return -1;
  }
}

int nxnet_socket_tcp_start(NxnetSocket *self, unsigned short port) {
  self->address.sin_family = AF_INET;
  self->address.sin_addr.s_addr = INADDR_ANY;
  self->address.sin_port = htons(port);

  int opt = 1;

  int tcp_sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (tcp_sock < 0) {
    perror("<!> ERROR creating socket");
    return (-1);
  }

  int opt_err = setsockopt(tcp_sock, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
                           &opt, sizeof(opt));
  if (opt_err) {
    perror("<!> ERROR setting socket options");
    return (-1);
  }
  fcntl(tcp_sock, F_SETFL, O_NONBLOCK);

  int bind_err =
      bind(tcp_sock, (struct sockaddr *)&self->address, sizeof(self->address));
  if (bind_err < 0) {
    perror("<!> ERROR binding socket");
    return (-1);
  }

  int lis_err = listen(tcp_sock, 16);
  if (lis_err < 0) {
    printf("<!> ERROR setting up listen queue\n");
    return (-1);
  }
  self->kind = NXNET_SOCKET_SERVER;
  self->running = 1;
  self->fd = tcp_sock;
  self->port = port;
  return (tcp_sock);
}

int _nxnet_socket_accept(NxnetSocket *self) {
  int addrlen = sizeof(self->address);
  int new_socket = accept(self->fd, (struct sockaddr *)&self->address,
                          (socklen_t *)&addrlen);
  if (new_socket < 0) {
    return -1;
  } else {
    fcntl(new_socket, F_SETFL, O_NONBLOCK);
    for (int i = 0; i < stb_sb_count(self->connections); i++) {
      if (self->connections[i] == -1) {
        self->connections[i] = new_socket;
        nxnet_message_buffer_add(
            &self->messages,
            nxnet_makeNxnetMessage(new_socket, NXNET_MESSAGE_CONNECT, ""));
        return new_socket;
      }
    }
    stb_sb_push(self->connections, new_socket);
    nxnet_message_buffer_add(
        &self->messages,
        nxnet_makeNxnetMessage(new_socket, NXNET_MESSAGE_CONNECT, ""));
    return new_socket;
  }
}

void _nxnet_socket_receive(NxnetSocket *self) {

  int valread;

  if (self->kind == NXNET_SOCKET_SERVER) {
    for (int i = 0; i < stb_sb_count(self->connections); i++) {
      int connection = self->connections[i];
      if (connection >= 0 && !_nxnet_has_disconnection(self, i)) {
        char buffer[NXNET_READLEN + 1] = {0};
        valread = read(connection, buffer, NXNET_READLEN);
        if (!valread) {
          close(connection);
          _nxnet_socket_disconnection(self, connection);
        } else if (valread > 0) {
          nxnet_message_buffer_add(
              &self->messages,
              nxnet_makeNxnetMessage(connection, NXNET_MESSAGE_DATA, buffer));
        }
      }
    }
  } else if (self->kind == NXNET_SOCKET_CLIENT) {
    char buffer[NXNET_READLEN + 1] = {0};
    valread = read(self->fd, buffer, NXNET_READLEN);
    nxnet_message_buffer_add(
        &self->messages,
        nxnet_makeNxnetMessage(self->fd, NXNET_MESSAGE_DATA, buffer));
  }
}

void nxnet_socket_send(NxnetSocket *self, const char *data, int target) {
  if (self->running) {
    if (self->kind == NXNET_SOCKET_SERVER) {
      if (target == -1) {
        for (int i = 0; i < stb_sb_count(self->connections); i++) {
          int connection = self->connections[i];
          if (connection >= 0)
            nxnet_socket_send(self, data, connection);
        }
      } else if (target >= 0) {
        if (!_nxnet_has_disconnection(self, target)) {
          send(target, data, strlen(data), 0);
        } else {
          fprintf(
              stderr,
              "<!> WARNING: tried to send message to new connection (%i) "
              "without acknowledging loss of previous connection - sending was "
              "blocked to prevent accidental sending to incorrect remote\n",
              target);
        }
      }
    } else if (self->kind == NXNET_SOCKET_CLIENT) {
      send(self->fd, data, strlen(data), 0);
    }
  }
}

void nxnet_socket_tick(NxnetSocket *self) {
  if (self->running) {
    if (self->kind == NXNET_SOCKET_SERVER) {
      while (_nxnet_socket_accept(self) >= 0)
        ;
      _nxnet_socket_receive(self);
    } else if (NXNET_SOCKET_CLIENT) {
      _nxnet_socket_receive(self);
    }
  } else {
  }
}

#endif