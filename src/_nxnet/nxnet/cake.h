#ifndef CAKE_H
#define CAKE_H

#include <stdio.h>

void bake_cake(const char *flavour) { printf("fresh %s cake\n", flavour); }

#endif