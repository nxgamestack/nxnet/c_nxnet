#ifndef NXNET_MESSAGE_H
#define NXNET_MESSAGE_H

#include <stdlib.h>
#include <string.h>

typedef enum NxnetMessageKind {
  NXNET_MESSAGE_NULL,
  NXNET_MESSAGE_CONNECT,
  NXNET_MESSAGE_DISCONNECT,
  NXNET_MESSAGE_DATA,

} NxnetMessageKind;

typedef struct NxnetMessage {
  NxnetMessageKind kind;
  int source;
  char data[128 + 1];
} NxnetMessage;

NxnetMessage nxnet_makeNxnetMessage(int source, NxnetMessageKind kind,
                                    const char *data) {
  NxnetMessage self;
  self.kind = kind;
  self.source = source;
  // self.data = (char *)malloc(sizeof(char *) * (strlen(data) + 1));
  strcpy(self.data, data);
  return self;
}

/*
NxnetMessage *nxnet_newNxnetMessage(int source, const char *data) {
  NxnetMessage *self = (NxnetMessage *)malloc(sizeof(*self));
  *self = nxnet_makeNxnetMessage(source, data);
  return self;
}
*/

#endif