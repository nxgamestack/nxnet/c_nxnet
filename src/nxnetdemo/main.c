//#include <stdlib.h>
//#include <string.h>

#include "nxnet/api/prefix_net.h"
#include <stdio.h>

int main(int argc, char const *argv[]) {
  int sock = nxnet_start_tcp(8080);

  printf(">>>>%i\n", sock);

  while (nxnet_tick(sock)) {
    while (nxnet_receive(sock)) {
      int kind = nxnet_kind(sock);
      int source = nxnet_source(sock);
      char *data = nxnet_data(sock);

      switch (kind) {
      case NXNET_MESSAGE_DATA:
        printf("<-> msg (%i)>%s<\n", source, data);
        nxnet_send(sock, data, source);
        if (strcmp(data, "bye\n") == 0) {
          nxnet_close(sock);
        }
        break;
      case NXNET_MESSAGE_DISCONNECT:
        printf("<-> gon (%i)\n", source);
        nxnet_ack_disconnect(sock, source);
        break;
      case NXNET_MESSAGE_CONNECT:
        printf("<-> new (%i)\n", source);
        break;
      }
    }
  }

  nxnet_free_all();

  return 0;
}