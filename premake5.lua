require("mingw64")

workspace "WS_nxnet"
    configurations { "Debug", "Release" }
    platforms { "l64", "w64", "larm64"}
    filter { "platforms:l64" }
        system "linux"
        architecture "x64"

    filter { "platforms:larm64" }
        system "linux"
        architecture "ARM"

    filter { "platforms:w64" }
        system "windows"
        architecture "x64"
        toolset ("mingw64")
        --entrypoint "WinMainCRTStartup"

project "nxnet"
    language "C"
    
    kind "SharedLib"
    targetdir "lib/%{cfg.shortname}/libnxnet/"

    libdirs { "./trd/lib" }
    includedirs { "./trd/include" }

    --links { "GL", "glfw", "m"}
    

    files { "./src/_nxnet/**.h", "./src/_nxnet/**.c"}

    filter "configurations:Debug"
        defines { "DEBUG" }
        symbols "On"
        --warnings "Extra"
        enablewarnings { "all" }

    filter "configurations:Release"
        defines { "NDEBUG" }
        optimize "On"


    filter { "platforms:l64" }

    filter { "platforms:w64" }
    

project "nxnetdemo"
    language "C"
    kind "WindowedApp"
    targetdir "bin/%{cfg.shortname}/nxnetdemo/"

    libdirs { "./lib/%{cfg.shortname}/libnxnet/" }
    includedirs { "./src/_nxnet/", "./trd/include" }

    links { "nxnet" }

    files { "./src/nxnetdemo/**.h", "./src/nxnetdemo/**.c" }
    --links { "GL", "glfw", "m"}

    filter "configurations:Debug"
        defines { "DEBUG" }
        symbols "On"
        enablewarnings { "all" }
        disablewarnings { "parentheses" }

    filter "configurations:Release"
        defines { "NDEBUG" }
        optimize "On"

    filter { "platforms:l64" }

    filter { "platforms:w64" }
